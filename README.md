# Pitch for HypeMachine
A Chrome extension for HypeMachine.

## Browser support
The extension will eventually be available for more than just Chrome but most of the development is focused on Chrome for the moment.

## Progress & todos
You can learn a little about what is planned for the extension and request features using our [Trello board](https://trello.com/b/gdrOmLhV/pitch).

## Issues
If you experience a bug with the extension, please [open an issue](https://github.com/awfy/pitch/issues/new) on this repository or, better yet, fix it yourself by becoming a contributor.

## Contributing

Contributions towards Pitch are always welcome. The extension will need a lot of work to maintain across more than just Chrome. If you're interested then please drop me an email - [me@gre.gs](mailto:me@gre.gs).
